﻿using BlogASP.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogASP.Controllers
{
    public class ArticleController : Controller
    {
        /// <summary>
        /// Champ qui va permettre d'appeler des méthodes pour faire des actions sur notre fichier
        /// </summary>
        private readonly ArticleJSONRepository _repository;

        /// <summary>
        /// Constructeur par défaut, permet d'initialiser le chemin du fichier JSON
        /// </summary>
        public ArticleController()
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data", "liste_article_tuto_full.json");
            _repository = new ArticleJSONRepository(path);
        }

        // GET: List
        public ActionResult List()
        {
            try
            {
                List<Article> liste = _repository.GetAllListArticle().ToList();
                return View(liste);
            }
            catch
            {
                return View(new List<Article>());
            }
        }
    }
}